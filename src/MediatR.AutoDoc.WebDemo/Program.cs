using MediatR.AutoDoc.Blazor;
using MediatR.AutoDoc.Tests.Orders.Customer;
using MediatR.AutoDoc.Tests.Payments.Payment;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddMediatorAutoDoc(
    "GeneDoc Demo", 4,
    typeof(CreateCustomer).Assembly,
    typeof(ProcessOrderPayment).Assembly);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();