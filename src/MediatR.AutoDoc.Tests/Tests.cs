﻿using FluentAssertions;
using MediatR.AutoDoc.Core;
using MediatR.AutoDoc.Tests.Orders.Customer;
using MediatR.AutoDoc.Tests.Payments.Payment;
using Xunit;

namespace MediatR.AutoDoc.Tests;

public class Tests
{
    [Fact]
    public async Task GeneratorTest()
    {
        // Arrange
        var gen = new MediatorAutoDocGenerator("Test project", 4, typeof(CreateCustomer).Assembly, typeof(ProcessOrderPayment).Assembly);

        // Act
        var docs = await gen.Process(CancellationToken.None);

        // Assert
        docs.Should().NotBeNull();
        docs.Title.Should().Be("Test project");
        docs.SubSystems.Should().HaveCount(2);
        var ss1 = docs.SubSystems[0];
        var ss2 = docs.SubSystems[1];
        
        ss1.Title.Should().Be("Orders & Customers", "Taken from title of assembly");
        ss2.Title.Should().Be("Payments", "Taken from title of assembly");
        ss1.Readme.Should().Be("Manages orders and customers. Tracks orders amounts and delivery.");
        ss1.Areas.Should().HaveCount(2);
        ss1.Areas[0].Title.Should().Be("Customer");
        // ss1.Areas[0].Readme.Should().Be("# Customer");
        ss1.Areas[1].Title.Should().Be("Order");
        ss2.Areas.Should().HaveCount(1);
    }
}