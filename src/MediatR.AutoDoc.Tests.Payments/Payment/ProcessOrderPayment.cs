﻿using AutoMapper;
using FluentValidation;

namespace MediatR.AutoDoc.Tests.Payments.Payment;

/// <summary>
/// Process new payment for order. Marks order as payed and sends notification. See <see cref="Order" />
/// </summary>
public static class ProcessOrderPayment
{
    /// <summary>
    /// Order payment command model
    /// </summary>
    public class Command : IRequest<int>
    {
        /// <summary>
        /// Order id
        /// </summary>
        public int OrderId { get; set; }
        
        /// <summary>
        /// Payment amount
        /// </summary>
        public decimal PaymentAmount { get; set; }
        
        /// <summary>
        /// Code of payment currency in ISO-3
        /// </summary>
        public string CurrencyCode { get; set; }
    }

    public class Handler : IRequestHandler<Command, int>
    {
        public Task<int> Handle(Command request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }

    public class MapperProfile : Profile
    {
    } 
    
    public class Validator : AbstractValidator<Command>
    {
    }
}