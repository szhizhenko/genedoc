﻿namespace MediatR.AutoDoc.Tests.Orders.Order.Notifications;

/// <summary>
/// Fires when new order is places
/// </summary>
public class OrderPlacedNotification : INotification
{
    /// <summary>
    /// ID of placed order
    /// </summary>
    public int OrderId { get; set; }
}