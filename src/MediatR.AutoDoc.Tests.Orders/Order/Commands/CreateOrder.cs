﻿namespace MediatR.AutoDoc.Tests.Orders.Order.Commands;

/// <summary>
/// Places new order
/// </summary>
public abstract class CreateOrder
{
    /// <summary>
    /// New order parameters
    /// </summary>
    public class Command : IRequest<int>
    {
        /// <summary>
        /// Name of customer
        /// </summary>
        public string CustomerFullName { get; set; }
        
        /// <summary>
        /// Order's amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Cctor for command
        /// </summary>
        /// <param name="customerFullName"></param>
        public Command(string customerFullName)
        {
            CustomerFullName = customerFullName;
        }
    }

    /// <summary>
    /// New order command handler
    /// </summary>
    public class Handler : IRequestHandler<Command, int>
    {
        /// <summary>
        /// Process order creation command
        /// </summary>
        /// <param name="request">Parameters</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>ID of newly placed order</returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<int> Handle(Command request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}