﻿namespace MediatR.AutoDoc.Tests.Orders.Order.Queries;

/// <summary>
/// Used to find orders by filter
/// </summary>
public abstract class ListOrders
{
    public class Query : IRequest<Model[]>
    {
        /// <summary>
        /// Order ID. If specified, search will be performed by ID
        /// </summary>
        public int? OrderId { get; set; }
        
        /// <summary>
        /// Customer's name
        /// </summary>
        public string? CustomerFullName { get; set; }
        
        /// <summary>
        /// Min amount of order
        /// </summary>
        public decimal? AmountFrom { get; set; }
        
        /// <summary>
        /// Max amount of order
        /// </summary>
        public decimal? AmountTo { get; set; }
    }

    /// <summary>
    /// Order search result
    /// </summary>
    public class Model
    {
        /// <summary>
        /// Order ID
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Customer's full name
        /// </summary>
        public string CustomerFullName { get; set; }
        
        /// <summary>
        /// Amount of order
        /// </summary>
        public decimal Amount { get; set; }
    }

    public class Handler : IRequestHandler<Query, Model[]>
    {
        public Task<Model[]> Handle(Query request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}