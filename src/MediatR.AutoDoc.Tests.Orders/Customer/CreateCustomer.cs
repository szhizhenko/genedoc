﻿using AutoMapper;
using FluentValidation;

namespace MediatR.AutoDoc.Tests.Orders.Customer;

/// <summary>
/// Creates new customer. Both full name and phones must be unique.
/// </summary>
public abstract class CreateCustomer
{
    public class Command : IRequest<int>
    {
        /// <summary>
        /// Full name
        /// </summary>
        public string FullName { get; }
        
        /// <summary>
        /// Phone number
        /// </summary>
        public string? Phone { get; set; }

        public Command(string fullName)
        {
            FullName = fullName;
        }
    }

    public class Handler : IRequestHandler<Command, int>
    {
        public Task<int> Handle(Command request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }

    public class MapperProfile : Profile
    {
    } 
    
    public class Validator : AbstractValidator<Command>
    {
    }
}