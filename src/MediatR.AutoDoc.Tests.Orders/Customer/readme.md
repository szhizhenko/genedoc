﻿# Customer

This is the customer who places the order. Has simple notation:
```csharp
class Customer {
  public int Id {get;set;}
  public string Name {get;set;}
}
```