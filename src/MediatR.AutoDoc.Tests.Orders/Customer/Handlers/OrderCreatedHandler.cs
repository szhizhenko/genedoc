﻿using MediatR.AutoDoc.Tests.Orders.Order.Notifications;

namespace MediatR.AutoDoc.Tests.Orders.Customer.Handlers;

/// <summary>
/// Updates total amount in customer index when new order is placed
/// </summary>
public class OrderCreatedHandler : INotificationHandler<OrderPlacedNotification>
{
    public Task Handle(OrderPlacedNotification notification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}