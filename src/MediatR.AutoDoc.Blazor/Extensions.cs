﻿using System.Reflection;
using MediatR.AutoDoc.Core;
using Microsoft.Extensions.DependencyInjection;

namespace MediatR.AutoDoc.Blazor;

public static class Extensions
{
    public static IServiceCollection AddMediatorAutoDoc(this IServiceCollection serviceCollection, string projectTitle, int subSystemNamespaceDepth = 3, params Assembly[] assemblies)
    {
        serviceCollection.AddSingleton(new MediatorAutoDocGenerator(projectTitle, subSystemNamespaceDepth, assemblies));
        return serviceCollection;
    }
}