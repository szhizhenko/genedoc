﻿using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace MediatR.AutoDoc.Core.XmlDoc;

public class XmlDocReader
{
    private readonly Dictionary<string, XmlDocument> _docs = new();

    public string? GetSummary(Type type, string? memberName = null)
    {
        var assemblyName = type.Assembly.GetName().Name;
        if (string.IsNullOrEmpty(assemblyName))
            throw new ArgumentException("Unable to determine assembly name");

        XmlDocument? docRoot = null;
        if (!_docs.TryGetValue(assemblyName, out var doc))
        {
            var filePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var fileProbes = new[] { assemblyName + ".xml", Path.Combine(filePath, assemblyName + ".xml") };
            var fileName = fileProbes.FirstOrDefault(File.Exists);
#if DEBUG
            if (fileName == null)
                return $"No documentation file found: {string.Join("; ", fileProbes)}";
#else
            if (fileName == null)
                return null;
#endif
            docRoot = new XmlDocument();
            docRoot.LoadXml(File.ReadAllText(fileName));
            _docs.Add(assemblyName, docRoot);
        }
        else
        {
            docRoot = doc;
        }

        string? returnValue;
        if (string.IsNullOrEmpty(memberName))
        {
            var node = docRoot.SelectSingleNode($"/doc/members/member[@name=\"T:{type.FullName}\"]/summary");
            returnValue = node?.InnerText;
        } else {
            var node = docRoot.SelectSingleNode($"/doc/members/member[@name=\"P:{type.FullName + "." + memberName}\"]/summary");
            returnValue = node?.InnerText;
        }
        
        #if DEBUG
        return returnValue ?? $"!!! Unable to find documentation for {type.FullName}.{memberName}";
        #else
        return returnValue;
        #endif
    }
}