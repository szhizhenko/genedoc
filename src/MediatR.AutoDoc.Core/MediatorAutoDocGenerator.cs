﻿using System.Reflection;
using MediatR.AutoDoc.Core.Documentation;
using MediatR.AutoDoc.Core.XmlDoc;

namespace MediatR.AutoDoc.Core;

/// <summary>
/// MediatR documentation generator
/// </summary>
public class MediatorAutoDocGenerator
{
    private readonly string _projectTitle;
    private readonly int _subSystemNamespaceDepth;
    private readonly Assembly[] _assembliesToProcess;
    private readonly XmlDocReader _xmlDocReader;

    /// <summary>
    /// Constructs new instance of generator
    /// </summary>
    /// <param name="projectTitle">Project's name</param>
    /// <param name="subSystemNamespaceDepth">Depth of namespace parts to distinguish namespaces</param>
    /// <param name="assembliesToProcess">List of assemblies where to search notifications, request and notification handlers to document them</param>
    public MediatorAutoDocGenerator(string projectTitle, int subSystemNamespaceDepth = 3, params Assembly[] assembliesToProcess)
    {
        _projectTitle = projectTitle;
        _subSystemNamespaceDepth = subSystemNamespaceDepth;
        _assembliesToProcess = assembliesToProcess.Distinct().ToArray();
        _xmlDocReader = new XmlDocReader();
    }

    /// <summary>
    /// Processes search of specified assemblies for MediatR components and generates structured documentation class with descriptions of all components.
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task<ProjectDocumentation> Process(CancellationToken cancellationToken = default)
    {
        var projectDocumentation = new ProjectDocumentation(_projectTitle);
        foreach (var assembly in _assembliesToProcess)
        {
            var types = assembly.GetTypes();
            foreach (var type in types.OrderBy(t => t.FullName))
            {
                var shouldProcessType =
                    type.IsAssignableFrom(typeof(INotification))
                    || type.GetInterfaces().Any(x =>
                        x.IsGenericType && (x.GetGenericTypeDefinition() == typeof(INotificationHandler<>) ||
                                            x.GetGenericTypeDefinition() == typeof(IRequestHandler<,>)));
                if (!shouldProcessType || string.IsNullOrEmpty(type.Namespace))
                    continue;

                var namespaceParts = type.Namespace.Split('.').ToArray();
                var subSystemNamespace = string.Join(".", namespaceParts.Take(_subSystemNamespaceDepth));
                var subSystem = projectDocumentation.SubSystems.FirstOrDefault(ss => ss.Namespace == subSystemNamespace);
                if (subSystem == null)
                {
                    subSystem = new SubSystemDocumentation(assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description ?? subSystemNamespace,
                        subSystemNamespace)
                    {
                        Readme = GetReadme(assembly, subSystemNamespace)
                    };
                    projectDocumentation.SubSystems.Add(subSystem);
                }

                var areaNamespace = string.Join(".", namespaceParts.Take(_subSystemNamespaceDepth + 1));
                var area = subSystem.Areas.FirstOrDefault(ar => ar.Namespace == areaNamespace);
                if (area == null)
                {
                    area = new AreaDocumentation(namespaceParts.Skip(_subSystemNamespaceDepth).Take(1).FirstOrDefault(), areaNamespace)
                    {
                        Readme = GetReadme(assembly, areaNamespace)
                    };
                    subSystem.Areas.Add(area);
                }

                ProcessType(assembly, subSystem, area, type);
            }
        }

        return Task.FromResult(projectDocumentation);
    }

    private void ProcessType(Assembly assembly, SubSystemDocumentation subSystem, AreaDocumentation area, Type type)
    {
        if (type.IsAssignableFrom(typeof(INotification)))
        {
            ProcessNotification(subSystem, area, type);
        }
        else
        {
            var notificationHandlerType =
                type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(INotificationHandler<>));
            if (notificationHandlerType != null)
            {
                ProcessNotificationHandler(subSystem, area, type, notificationHandlerType);
            }
            else
            {
                var requestHandlerType =
                    type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IRequestHandler<,>));
                if (requestHandlerType != null)
                    ProcessRequestHandler(assembly, subSystem, area, type, requestHandlerType);
            }
        }
    }

    private void ProcessRequestHandler(Assembly assembly, SubSystemDocumentation subSystem, AreaDocumentation area, Type sourceType, Type requestHandlerType)
    {
        var arguments = requestHandlerType.GetGenericArguments();
        if (arguments.Length != 2)
            throw new ArgumentException($"Wrong request handler type: it should contain 2 generic arguments, but found {arguments.Length}");
        var summary = _xmlDocReader.GetSummary(sourceType.DeclaringType ?? sourceType);
        var readme = GetReadme(assembly, requestHandlerType.DeclaringType?.Namespace ?? requestHandlerType.Namespace);
        var query = BuildClassDocumentation("Request", subSystem, area, arguments[0]);
        var response = BuildClassDocumentation("Response", subSystem, area, arguments[1]);
        area.CommandsAndQueries.Add(new CommandQueryDocumentation(sourceType.DeclaringType?.Name ?? sourceType.Name)
        {
            Summary = summary,
            Readme = readme,
            Query = query,
            Response = response
        });
    }

    /// <summary>
    /// Obtain readable type name
    /// </summary>
    /// <param name="sourceType">Type to process</param>
    /// <returns>If type has declaring type, returns declaring type name together with source type name (for ex. SomeAction.Command)</returns>
    private string GetTypeName(Type sourceType)
    {
        if (sourceType.DeclaringType != null)
            return $"{sourceType.DeclaringType.Name}.{sourceType.Name}";
        return sourceType.Name;
    }


    /// <summary>
    /// Construct documentation section for whole class
    /// </summary>
    /// <param name="parameterName">Name of parameter if this class is parameter of some other class</param>
    /// <param name="subSystem"></param>
    /// <param name="area"></param>
    /// <param name="classType"></param>
    /// <returns></returns>
    private ParameterDocumentation BuildClassDocumentation(string parameterName, SubSystemDocumentation subSystem, AreaDocumentation area, Type classType)
    {
        if (classType.IsPrimitive)
        {
            return new ParameterDocumentation(parameterName, classType);
        }

        // TODO Iterate public members
        return new ParameterDocumentation(parameterName, classType)
        {
            Summary = _xmlDocReader.GetSummary(classType.DeclaringType ?? classType),
        };
    }

    private void ProcessNotificationHandler(SubSystemDocumentation subSystem, AreaDocumentation area, Type sourceType,
        Type notificationHandlerType)
    {
        var arguments = notificationHandlerType.GetGenericArguments();
        area.NotificationHandlers.Add(
            new NotificationHandlerDocumentation(
                GetTypeName(sourceType.DeclaringType ?? sourceType), BuildClassDocumentation("Notification", subSystem, area, arguments[0]))
            {
                Summary = _xmlDocReader.GetSummary(sourceType.DeclaringType ?? sourceType)
            });
    }

    private void ProcessNotification(SubSystemDocumentation subSystem, AreaDocumentation area, Type notificationType)
    {
        throw new NotImplementedException();
    }

    #region Private methods

    private string? GetReadme(Assembly assembly, string? @namespace)
    {
        if (string.IsNullOrEmpty(@namespace))
            return null;

        var resourceStream = assembly.GetManifestResourceStream(@namespace + ".readme.md");
        if (resourceStream != null)
        {
            using var sr = new StreamReader(resourceStream);
            return sr.ReadToEnd();
        }

        return null;
    }

    #endregion
}