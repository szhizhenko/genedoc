﻿namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Documentation for notification handler
/// </summary>
public class NotificationHandlerDocumentation
{
    /// <summary>
    /// Name of notification handler
    /// </summary>
    public string Title { get; }
    
    /// <summary>
    /// Summary for handler
    /// </summary>
    public string? Summary { get; set; }
    
    /// <summary>
    /// Notification description
    /// </summary>
    public ParameterDocumentation Notification { get; }

    public NotificationHandlerDocumentation(string title, ParameterDocumentation notification)
    {
        Title = title;
        Notification = notification;
    }
}