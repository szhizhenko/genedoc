﻿using System.Diagnostics;

namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Документация по предметной области (сущности или набора сущностей)
/// </summary>
[DebuggerDisplay("Area: {Title} ({Namespace})")]
public class AreaDocumentation
{
    /// <summary>
    /// Заголовок предметной области
    /// </summary>
    public string? Title { get; set; }
    
    /// <summary>
    /// Пространство имён предметной области
    /// </summary>
    public string Namespace { get; set; }
    
    /// <summary>
    /// Информация из readme.md
    /// </summary>
    public string? Readme { get; set; }

    /// <summary>
    /// Документация по командам и запросам
    /// </summary>
    public List<CommandQueryDocumentation> CommandsAndQueries { get; } = new();

    /// <summary>
    /// Документация по событиям области
    /// </summary>
    public List<ParameterDocumentation> Notifications { get; } = new();

    /// <summary>
    /// Обработчики событий
    /// </summary>
    public List<NotificationHandlerDocumentation> NotificationHandlers { get; } = new();

    public AreaDocumentation(string? title, string ns)
    {
        Title = title;
        Namespace = ns;
    }
}