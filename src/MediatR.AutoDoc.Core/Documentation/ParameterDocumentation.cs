﻿using System.Diagnostics;

namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Документация параметра класса
/// </summary>
[DebuggerDisplay("Parameter: {Name} of type {Type}")]
public class ParameterDocumentation {
    /// <summary>
    /// Название параметра
    /// </summary>
    public string Name { get; }
    
    /// <summary>
    /// Тип параметра
    /// </summary>
    public Type Type { get; }

    /// <summary>
    /// Детальное описание типа параметра, если тип сложный
    /// </summary>
    public List<ParameterDocumentation> TypeDocumentation { get; } = new();

    /// <summary>
    /// Описание параметра
    /// </summary>
    public string? Summary { get; set; }

    public ParameterDocumentation(string name, Type type)
    {
        Name = name;
        Type = type;
    }
}