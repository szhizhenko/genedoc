﻿using System.Diagnostics;

namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Документация по подсистеме
/// </summary>
[DebuggerDisplay("SubSystem: {Title} ({Namespace})")]
public class SubSystemDocumentation
{
    /// <summary>
    /// Название подсистемы
    /// </summary>
    public string Title { get; set; }
    
    /// <summary>
    /// Пространство имён подсистемы. Ограниченной вложенностью (по-умолчанию 3)
    /// </summary>
    public string Namespace { get; set; }
    
    /// <summary>
    /// Файл документации подсистемы
    /// </summary>
    public string? Readme { get; set; }

    /// <summary>
    /// Предметные области подсистемы (сущности)
    /// </summary>
    public List<AreaDocumentation> Areas { get; set; } = new();

    public SubSystemDocumentation(string title, string @namespace)
    {
        Title = title;
        Namespace = @namespace;
    }
}