﻿using System.Diagnostics;

namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Документация по конкретной команде или запросу
/// </summary>
[DebuggerDisplay("CommandQuery: {Title}")] 
public class CommandQueryDocumentation
{
    /// <summary>
    /// Заголовок команды/запроса
    /// </summary>
    public string Title { get; set; }
    
    /// <summary>
    /// Комментарий к обобщающему классу команды или запроса
    /// </summary>
    public string? Summary { get; set; }
    
    /// <summary>
    /// Дополнительная информация из файла readme.md
    /// </summary>
    public string? Readme { get; set; }
    
    /// <summary>
    /// Документация к модели запроса
    /// </summary>
    public ParameterDocumentation? Query { get; set; }
    
    /// <summary>
    /// Документация к модели ответа
    /// </summary>
    public ParameterDocumentation? Response { get; set; }

    public CommandQueryDocumentation(string title)
    {
        Title = title;
    }
}