﻿using System.Diagnostics;

namespace MediatR.AutoDoc.Core.Documentation;

/// <summary>
/// Документация по всему проекту целиком
/// </summary>
[DebuggerDisplay("Project: {Title}")]
public class ProjectDocumentation
{
    /// <summary>
    /// Название проекта. Указывается глобально на уровне генератора документации
    /// </summary>
    public string Title { get; }

    /// <summary>
    /// Подсистемы 
    /// </summary>
    public List<SubSystemDocumentation> SubSystems { get; } = new();

    public ProjectDocumentation(string title)
    {
        Title = title;
    }
}