# MediatR AutoDoc
This library automatiacally generates documentation based on MediatR handlers.
Just use as:
```csharp
builder.Services.UseGeneDoc(
    "Global Project Name", 
    typeof(CreateCustomer).Assembly,
    typeof(ProcessOrderPayment).Assembly);
```

And then use generator to get object model of documentation:
```scharp
var generator = new GeneDocGenerator(projectTitle, 3, assemblies);
var documentation = generator.Process();
```

# Blazor component
You can also use blazor component to render documentation at any blazor page.
Include this using in _Imports.razor:
```razor
@using MediatR.AutoDoc.Blazor.Components
```

And you ready to use documentation renderer component, add it to any page:
```razor
<AutoDocDocumentation></AutoDocDocumentation>
```
And that's it.